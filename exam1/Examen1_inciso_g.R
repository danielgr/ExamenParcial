#DanielGallegos
#Primer Parcial

require(ggplot2)
require(matrixStats)
setwd('~/Documents/ITESO/Septimo Semestre/Modelos_de_Credito/ExamenParcial/exam1')

revenue <- 10000 * accepted_bin * (1 - default_bin) * array_amount_good * array_total_interest
default_cost <- 10000 * accepted_bin * default_bin * array_amount_bad * array_default
marketing    <- 1000000
utilities    <- revenue - default_cost 

plot(score_cuts, utilities, type = 'l')