#DanielGallegos
#Primer Parcial


#!/usr/bin/env Rscript

setwd('~/Documents/ITESO/Septimo Semestre/Modelos_de_Credito/RepositorioJorge/Modelos-de-Credito-Primavera2016/exam1')
df <- read.csv('data_exam.csv', head=T)

vars_model <- c('credit_history_numeric', 'time_credit_acc', 'amount', 'savings_acc_numeric', 
                'p_employment_time_numeric', 'installment_rate', 'p_residence_time', 'age', 'number_of_credits',
                'has_phone_numeric', 'foreign_worker_numeric')


set.seed(123456)
#crea una muestra del 60% de los datos
selected_rows <- sort(sample(nrow(df),floor(0.6*nrow(df)))) 

data_model <- df[selected_rows,c(vars_model,'is_good')]
data_test <- df[-selected_rows,c(vars_model,'is_good')]

data_model$is_good <- as.factor(data_model$is_good)

#rf <- randomForest(data_model$is_good ~ data_model$age + data_model$amount) 
rf <- randomForest(data_model$is_good ~ .,
                   data_model, 
                   mtry = ncol(data_model)-1,
                   ntree = 2*nrow(data_model)/3)


for (test_row in rownames(data_test)){
  data_test[test_row,'randomForest'] <- predict(rf,
                                                data_test[test_row,], type = 'prob')[2]
  
}

for (test_row in rownames(data_model)){
  data_model[test_row,'randomForest'] <- predict(rf,
                                                 data_model[test_row,], type = 'prob')[2]
  
}

breaks<-seq(from = 0, to = 1, by = 0.1)
hg<-hist(data_test[data_test$is_good==1,'randomForest'],
         breaks = breaks,
         col = rgb(0,0,1,0.5))

hb<-hist(data_test[data_test$is_good==2,'randomForest'], 
         breaks = breaks,
         col = rgb(1,0,0,0.5),
         add = T)
